
var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
const axios = require('axios');
app.set("view engine", "ejs");

app.listen(port, function(){
  console.log("Server started...")
});

var urlMlabRaiz ="https://api.mlab.com/api/1/databases/crodriguezv/collections/"
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMlabRaiz;

app.get('/', function(req, res){
  res.sendFile("index.html",{root: '.'})
})

app.get("/movimientos/:contrato", function(req, res){

  var productId = String(req.params.contrato).substring(4,8);
  var clientId = String(req.params.contrato).substring(0,4);
  var contractId = req.params.contrato

  var queryProducto = 'q={"id":"' + productId + '"}'
  var queryTransaction = 'q={"contrato":"' + contractId + '"}'
  var queryClient = 'q={"id":"' + clientId + '"}'
  var prodsData;
  var transactionsData;
  var clientContracts;
  var wantedContractData;

  axios.all([
    axios.get(urlMlabRaiz + "Productos" + apiKey + "&" + queryProducto),
    axios.get(urlMlabRaiz + "Transactions" + apiKey + "&" + queryTransaction),
    axios.get(urlMlabRaiz + "ClientesTest" + apiKey + "&" + queryClient)
  ])
  .then(axios.spread((prods, transactions, client) => {
    // handle success
    prodsData = prods.data[0];
    transactionsData = transactions.data;
    clientContracts = client.data[0].contratos

    for(var i = 0; i< clientContracts.length; i++){
      if(clientContracts[i].noContrato == contractId){
        wantedContractData = clientContracts[i]
      }
    }

    res.render("movimientos",{clientId: client.data[0].id, contractData : wantedContractData, prodsData: prodsData, transactionsData: transactionsData})
  }))
})

app.get("/compraVenta/:cliente",function(req, res){

  var clienteId = req.params.cliente;
  var queryClient = 'q={"id":"'+clienteId+'"}'
  var prodsData;
  var clientData;

  axios.all([
    axios.get(urlMlabRaiz + "Productos" + apiKey),
    axios.get(urlMlabRaiz + "ClientesTest" + apiKey + "&" + queryClient)
  ])
  .then(axios.spread((prods, client) => {
    prodsData = prods.data; //Lista de los productos
    clientData = client.data[0]; //Atributos del cliente

    var prodsListClient = []
    var prodsList = []
    //Se seleccionan los productos que actualmente tienen el cliente
    for(var i = 0; i<clientData.contratos.length; i++){
      prodsListClient.push(clientData.contratos[i].noContrato.substring(4));
    };
    //Se seleccionan los producto que el cliente no tiene.
    for(var i = 0; i<prodsData.length; i++){
      if(!prodsListClient.includes(prodsData[i].id)){
        prodsList.push(prodsData[i])
      }
    }
    res.render("compraVenta",{prodsList:prodsList, clientData:clientData})
  }))


})

app.get("/confirmacion/:operacion/:contrato/:importe/", function(req, res){
  var clienteId = (req.params.contrato).substring(0,4);
  var prodId = (req.params.contrato).substring(4);
  var importe = req.params.importe;
  var contrato = req.params.contrato;
  var oper = req.params.operacion;
  var queryProd = 'q={"id":"' + prodId + '"}'
  axios.all([
    axios.get(urlMlabRaiz + "Productos" + apiKey + "&" + queryProd),
  ])
  .then(axios.spread((prod) => {
    prodData = prod.data[0];
    if(oper == "Venta"){
      var total = parseFloat(importe)*prodData.precio;
      res.render("confirmacion",{prodData:prodData, contrato:contrato, oper:oper, importe:total});
    }else if(oper == "Compra"){
      res.render("confirmacion",{prodData:prodData, contrato:contrato, oper:oper, importe:importe});
    }else{
      res.sendFile("ooops.html",{root: '.'})
    }

  }))
})


app.get("/resultado/cliente/prod/importe/concepto/fecha", function(req, res){
  var cliente = req.params.cliente;
  var prod = req.params.prod;
  var importe = req.params.importe;
  var concepto = req.params.concepto;
  var fecha = req.params.fecha;
  var productQuery = 'q={"id":"'+ prod + '"}'

  var prodData1;
  var lastMove1;
  axios.all([
    axios.get(urlMlabRaiz + "Transactions" + apiKey),
    axios.get(urlMlabRaiz + "Productos" + apiKey + "&" + productQuery)

  ])
  .then(axios.spread((moves, prod) => {

    prodData = prod.data[0];
    lastMove = moves[moves.lenght-1].movementId

    res.render(resultado)
  }))


})
